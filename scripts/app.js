/*FUNCIONES*/

//Arrays con la información de todos los pokemons + array con la información de cada pokemon buscado. 
var arrIndicePokemon = new Array


function nFetch(url, nodo) {
    fetch(url)
        .then(function (respuesta) {
            return respuesta.json()
        })
        .then(function (respuesta) {
            var pokeApi = respuesta
            for (var i = 0; i < pokeApi.results.length; i++) {
                arrIndicePokemon.push(pokeApi.results[i])
                console.log(arrIndicePokemon)

                nodo.innerHTML += `<span><a href="#nombre">${pokeApi.results[i].name}</a></span> | `
            }
        })
}



//---------------------------------------------EMPIEZA LA INTERACCIÓN CON EL DOM-------------------------------------------

document.addEventListener("DOMContentLoaded", function () {


    //Manejamos el limite de la api hasta 808 que son todos los pokemons
    var urlPokeApi = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=806"
    var nodoLista = document.getElementById("listaHover")

    //Llamada a la funcion fetch y pinta el nombre de los pokemon
    nFetch(urlPokeApi, nodoLista)


    nodoLista.onclick = function (e) {
        //La siguiente linea previene que el click que dispara el evento sea el padre, solo permite que se dispare en el hijo
        if (e.target !== e.currentTarget) {
            let campoBusqueda = document.getElementById("nombre")
            campoBusqueda.value = e.target.innerHTML
            campoBusqueda.style.borderColor = "green"; //Cambia el borde del buscador para avisar al usuario
        }
    }

    var formulario = document.getElementById("formulario")

    formulario.addEventListener("submit", function (e) {

        //document.getElementById("nombre").style.borderColor = "rgba(32, 32, 32, 0.2);"; //reinicia el color del borde del buscador

        e.preventDefault()

        var busqueda = document.getElementById("nombre").value
        var urlPokemon //Declarada para poder acceder luego a ella. 



        for (var indice = 0; indice < arrIndicePokemon.length; indice++) {

            if (busqueda == (arrIndicePokemon[indice].name)) {
                var urlPokemon = arrIndicePokemon[indice].url
            }
            // Si la busqueda tiene mayusculas también entra. 
            else if (busqueda.toLowerCase() == (arrIndicePokemon[indice].name)) {

                var urlPokemon = arrIndicePokemon[indice].url

            }
        }

        /* Empieza segundo fetch */
        fetch(urlPokemon)
            .then(function (respuesta) {
                //console.log(respuesta) para saber el estado 200 o 404 

                if (respuesta.status !== 200) {
                    alert("Tu busqueda ha fallado, revisa que has escrito bien tu pokemon")
                }

                return respuesta.json()

            })
            .then(function (respuesta) {
                var pokemon = respuesta
                //console.log(pokemon) //para saber las caracteristicas que puedo seleccionar


                /* PLANTILLA SQUIRRELY */
                var myTemplate = "<div class='card w-100 color-type-{{types[0].type.name}} mb-5' style='width: 18rem;'>" +
                    "<img data-src1='{{sprites.front_default}}' data-src2='{{sprites.back_default}}' src='{{sprites.front_default}}' class='card-img-top imagenPokemon' alt='{{name}}'>" +
                    "<div class='card-body'><h5 class='card-title'><b>{{name}}</b></h5><p class='card-text'>" +
                    "<ul><h5>Estadísticas</h5>{{each(options.stats)}}<li>{{@this.stat.name}} Base: {{@this.base_stat}}</li>{{/each}}</ul>" +
                    "</p></div><ul class='list-group list-group-flush px-2'><li class='list-group-item'>Tipo: {{types[0].type.name}}</li>" +
                    "<li class='list-group-item'>Altura: {{height}}Dm</li><li class='list-group-item'>Peso: {{weight}}hg</li></ul>" +
                    "<div class='card-body'><a href='https://pokemon.fandom.com/es/wiki/{{name}}' target='_blank' class='card-link'>Wikidex</a></div></div></div>"



                var result = Sqrl.Render(myTemplate, pokemon)


                //Crea el elemento padre de la plantilla y le setea las classes de bootstrap
                var divPrincipal = document.createElement("div")
                divPrincipal.classList.add("col-sm-12", "col-md-6", "col-lg-4", "my-3")

                //Inserta el render de squirrelly de la plantilla
                var divPadre = document.getElementById("plantilla").parentElement
                divPadre.insertBefore(divPrincipal, divPadre.firstChild)
                divPrincipal.innerHTML = result


                var contenedorPokemons = document.getElementById("fila"); //seleccionamos el elemento padre

                /* Voltea la imagen para ver 2 sprites */
                contenedorPokemons.onclick = function (e) {

                    //alert(e.target.src) //Para comprobar si el evento click funciona

                    //e es el evento y al usar target solo disparamos la imagen clickada (por usar onclick en lugar de addEventListener)
                    if (e.target.src == e.target.dataset.src1 && (e.target.dataset.src2 !== "null")/*Evita que si no hay segunda imagen se cambia a null*/) {
                        e.stopPropagation();
                        return e.target.src = e.target.dataset.src2
                    }
                    if (e.target.src == e.target.dataset.src2) {
                        e.stopPropagation();
                        return e.target.src = e.target.dataset.src1
                    }

                    e.stopPropagation();

                }
            })
    })
})
