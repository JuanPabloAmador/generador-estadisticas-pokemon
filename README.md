# Generador estadisticas pokemon

Ejercicio para comprender como consumir una **API** con javascript,
haciendo uso de **promesas** y **fetch** API. 

Puede encontrar un Deploy del proyecto en el siguiente enlace:
**[http://comparadorpokemon.juanpabload.com/](http://comparadorpokemon.juanpabload.com/)**

Se utilizó la API publica: **[https://pokeapi.co/](https://pokeapi.co/)**.

La página web esta realizada con **html**, **css** + **bootstrap** y **javascript** básico.

En cuanto al funcionamiento, primero se ingresa de la pokeapi una lista
de todos los pokemon disponibles, la API en esa primera llamada provee 
una lista con el nombre de los pokemons y la url de la API de ese pokemon 
concreto. 
El buscador de la página funciona haciendo uso de los nombres de la lista, una
vez coincide el nombre buscado se realiza una segunda llamada a la API también
con la fetch de la url concreta del mismo.

Por otro lado una vez el pokemon se pinta en una card de bootstrap se crea
un **dataset** de html5 que guarda la información de la url de dos de las imágenes
del pokemon, con el fin de que el usuario pueda cambiar entre imágenes.
